<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    
    public function testProductList()
    {
        $response = $this->get('/product-list');
        $response->assertStatus(200);
    }

    public function testFeaturedList()
    {
        $response = $this->get('/featured-list');
        $response->assertStatus(200);
    }

    public function testProductShow()
    {
        $response = $this->get('/product-show/1');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            "id",
            "product_name",
            "product_desc",
            "product_price",
            "is_featured",
            "status",
            "created_at",
            "updated_at"
        ]);
    }

    public function testProductAdd()
    {   
        $payload = [
            "product_name" => "test",
            "product_desc" => "test",
            "product_price" => 2.12
        ];

        $response = $this->postJson("/product-add", $payload);
        $response->assertStatus(201);
        $response->assertJsonStructure([
            "id",
            "product_name",
            "product_desc",
            "product_price",
            "created_at",
            "updated_at"
        ]);
    }

    public function testProductEdit()
    {   
        $payload = [
            "product_name" => "test",
            "product_desc" => "test",
            "product_price" => 2
        ];

        $response = $this->putJson("/product-edit/1", $payload);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            "id",
            "product_name",
            "product_desc",
            "product_price",
            "created_at",
            "updated_at"
        ]);
    }
    
    public function testProductFeatured()
    {      
        $payload = [
            "product_name" => "test",
            "product_desc" => "test",
            "product_price" => 2,
            "is_featured" => 1,
        ];

        $response = $this->putJson("/product-edit/1", $payload);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            "id",
            "product_name",
            "product_desc",
            "product_price",
            "is_featured",
            "status",
            "created_at",
            "updated_at"
        ]);
    }

    public function testProductDelete()
    {   
        $response = $this->deleteJson("/product-delete/1");
        $response->assertStatus(200);
        $response->assertJsonStructure([
            "id",
            "product_name",
            "product_desc",
            "product_price",
            "is_featured",
            "status",
            "created_at",
            "updated_at"
        ]);
    }

}
