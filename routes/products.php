<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

Route::get('/product-list', [ProductController::class, 'getProducts']);
Route::get('/featured-list', [ProductController::class, 'getFeatured']);
Route::get('/product-show/{id}', [ProductController::class, 'showProduct']);
Route::post('/product-add', [ProductController::class, 'addProduct']);
Route::put('/product-edit/{id}', [ProductController::class, 'editProduct']);
Route::delete('/product-delete/{id}', [ProductController::class, 'deleteProduct']);