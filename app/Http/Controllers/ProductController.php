<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Requests\ProductRequest;

class ProductController extends Controller
{

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getProducts()
    {
        try {

            $get_products = cache()->remember('product-list', 60*60*24, function(){
               return $this->product->paginate(10);
            });

            return $get_products;

        } catch (\Exception $e) {
            return response()->json(array(
                "status" => "Error",
                "message" => $e->getMessage(),
            ));
        }
    }

    public function getFeatured()
    {
        try {

            $get_featured = $this->product->where("is_featured", 1)->paginate(5);
            return $get_featured;

        } catch (\Exception $e) {
            return response()->json(array(
                "status" => "Error",
                "message" => $e->getMessage(),
            ));
        }
    }

    public function showProduct($id)
    {
        try {

            $show_product = cache()->remember('product-show-'.$id, 60*60*24, function() use($id){
                return $this->product->findOrFail($id);
             });

            return $show_product;

        } catch (\Exception $e) {
            return response()->json(array(
                "status" => "Error",
                "message" => $e->getMessage(),
            ));
        }
    }

    public function addProduct(Request $request)
    {
        try {
            
            $request->validate([
                    'product_name' => 'required|max:255',
                    'product_desc' => 'required|max:500',
                    'product_price' => 'required',
                    'is_feature' => 'nullable|numeric',
                    'status' => 'nullable|numeric',
                ]
            );

            $insert_product = $this->product->create($request->all());

            if ($insert_product) {
                cache()->forget('product-list');
            }

            return $insert_product;

        } catch (\Exception $e) {
            return response()->json(array(
                "status" => "Error",
                "message" => $e->getMessage(),
            ));
        }
    }

    public function editProduct(Request $request, $id)
    {
        try {
            
            $request->validate([
                    'product_name' => 'required|max:255',
                    'product_desc' => 'required|max:500',
                    'product_price' => 'required',
                    'is_feature' => 'nullable|numeric',
                    'status' => 'nullable|numeric',
                ]
            );

            $edit_product = tap($this->product->findOrFail($id))->update($request->all());

            if ($edit_product) {
                if (cache()->has('product-show-'.$id)) {
                    cache()->forget('product-show-'.$id);
                }
            }

            return $edit_product;

        } catch (\Exception $e) {
            return response()->json(array(
                "status" => "Error",
                "message" => $e->getMessage(),
            ));
        }
    }

    public function deleteProduct($id)
    {
        try {
        
            $delete_product = tap($this->product->findOrFail($id))->delete();

            if ($delete_product) {
                if (cache()->has('product-show-'.$id)) {
                    cache()->forget('product-show-'.$id);
                }
                cache()->forget('product-list');
            }

            return $delete_product;

        } catch (\Exception $e) {
            return response()->json(array(
                "status" => "Error",
                "message" => $e->getMessage(),
            ));
        }
    }
}
